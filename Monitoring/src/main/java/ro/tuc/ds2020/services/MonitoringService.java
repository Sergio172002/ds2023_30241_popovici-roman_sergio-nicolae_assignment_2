package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.entities.MonitoringData;
import ro.tuc.ds2020.repositories.MonitoringRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MonitoringService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MonitoringService.class);
    private final MonitoringRepository monitoringRepository;

    @Autowired
    public MonitoringService(MonitoringRepository personRepository) {
        this.monitoringRepository = personRepository;
    }

    public List<PersonDTO> findPersons() {
        List<MonitoringData> personList = monitoringRepository.findAll();
        return personList.stream()
                .map(PersonBuilder::toPersonDTO)
                .collect(Collectors.toList());
    }

    public PersonDetailsDTO findPersonById(UUID id) {
        Optional<MonitoringData> prosumerOptional = monitoringRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(MonitoringData.class.getSimpleName() + " with id: " + id);
        }
        return PersonBuilder.toPersonDetailsDTO(prosumerOptional.get());
    }

    public UUID insert(PersonDetailsDTO personDTO) {
        MonitoringData person = PersonBuilder.toEntity(personDTO);
        person = monitoringRepository.save(person);
        LOGGER.debug("Person with id {} was inserted in db", person.getDevice_id());
        return person.getDevice_id();
    }

}
