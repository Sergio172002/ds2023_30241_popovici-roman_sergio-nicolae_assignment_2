package ro.tuc.ds2020.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import org.hibernate.annotations.Type;

import java.io.Serializable;
import java.util.UUID;

@Entity
public class MonitoringData  implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    private UUID device_id;

    @Column(name = "consumption", nullable = false)
    private Integer consumption;


    public MonitoringData() {
    }

    public MonitoringData(UUID id, Integer consumption) {
        this.device_id = id;
        this.consumption = consumption;

    }

    public UUID getDevice_id() {
        return device_id;
    }

    public void setDevice_id(UUID device_id) {
        this.device_id = device_id;
    }

    public Integer getConsumption() {
        return consumption;
    }

    public void setConsumption(Integer consumption) {
        this.consumption = consumption;
    }
}
