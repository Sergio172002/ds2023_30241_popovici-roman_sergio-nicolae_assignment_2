package ro.tuc.ds2020.dtos.RabbitMQComp;

import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.listener.RabbitListenerContainerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class RabbitMQConfig {

    @Primary
    @Bean(name = "sensorValuesConnectionFactory")
    public ConnectionFactory sensorValuesConnectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setHost("goose-01.rmq2.cloudamqp.com");
        connectionFactory.setPort(5672);
        connectionFactory.setUsername("fqmbnfna");
        connectionFactory.setPassword("e7jRtYGIh7sWaSP_LBJBrhqn3-mXMRi9");
        connectionFactory.setVirtualHost("fqmbnfna");
        return connectionFactory;
    }

    @Bean(name = "devicesQueueConnectionFactory")
    public ConnectionFactory devicesQueueConnectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        // Set the appropriate connection details for DevicesQueue
        connectionFactory.setHost("cow-01.rmq2.cloudamqp.com");
        connectionFactory.setPort(5672);
        connectionFactory.setUsername("vxcoohzc");
        connectionFactory.setPassword("WnfSe9y-b52vGBw6HyUeBogPADnGK1Qa");
        connectionFactory.setVirtualHost("vxcoohzc");
        return connectionFactory;
    }

    @Bean(name = "sensorValuesContainerFactory")
    public RabbitListenerContainerFactory<?> sensorValuesContainerFactory(@Qualifier("sensorValuesConnectionFactory") ConnectionFactory connectionFactory) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        // additional configuration for sensorValuesContainerFactory if needed
        return factory;
    }

    @Bean(name = "devicesQueueContainerFactory")
    public RabbitListenerContainerFactory<?> devicesQueueContainerFactory(@Qualifier("devicesQueueConnectionFactory") ConnectionFactory connectionFactory) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        // additional configuration for devicesQueueContainerFactory if needed
        return factory;
    }
}
