package ro.tuc.ds2020.dtos.RabbitMQComp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import ro.tuc.ds2020.entities.MonitoringData;
import ro.tuc.ds2020.repositories.MonitoringRepository;
import org.springframework.messaging.simp.SimpMessagingTemplate;


import java.util.*;

@Component
public class RabbitMQReceiver {

    private final MonitoringRepository monitoringRepository;

    private final  List<MonitoringData> devices = new ArrayList<>();

    private final Map<String, Double> hourlyConsumptionMap = new HashMap<>();

    private long lastTimestamp;
    private double lastMeasurement;

    private int NO_OF_MEASUREMENTS = 0;

    private static final int MEASUREMENTS_PER_HOUR = 5;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;


    public RabbitMQReceiver(MonitoringRepository monitoringRepository) {
        this.monitoringRepository = monitoringRepository;
    }


    @RabbitListener(queues = "SensorValues", containerFactory = "sensorValuesContainerFactory")
    public void receiveMessage(String json) {
        try {
            // Process the received message

            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, Object> sensorInfo = objectMapper.readValue(json, Map.class);

            // Extract relevant information from the sensor message
            String deviceIdFromSensor = (String) sensorInfo.get("device_id");
            long timestamp = (long) sensorInfo.get("timestamp");
            Double measurementValue = (Double) sensorInfo.get("measurement_value");

            calculateHourlyConsumption(deviceIdFromSensor, timestamp, measurementValue);

            //System.out.println("Received message from sensor queue: " + json);
        } catch (Exception e) {
            // Log any exceptions
            e.printStackTrace();
        }
    }

    @RabbitListener(queues = "DevicesQueue", containerFactory = "devicesQueueContainerFactory")
    public void receiveDevicesQueueMessage(String json) {
        try {
            // Process the received JSON message for DevicesQueue queue
            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, Object> deviceInfo = objectMapper.readValue(json, Map.class);

            // Print the device information
            System.out.println("Received message from DevicesQueue queue:");
            System.out.println("Device ID: " + deviceInfo.get("device_id"));
            System.out.println("Hour Consumption: " + deviceInfo.get("hour_consumption"));
            String receivedMessage = (String) deviceInfo.get("device_id");
            UUID deviceId = UUID.fromString(receivedMessage);
            MonitoringData device = new MonitoringData(deviceId, (Integer) deviceInfo.get("hour_consumption"));
            devices.add(device);
        } catch (Exception e) {
            // Log any exceptions
            e.printStackTrace();
        }
    }

    private void calculateHourlyConsumption(String deviceId, long timestamp, Double measurementValue) {
        if(!hourlyConsumptionMap.containsKey(deviceId)){
            hourlyConsumptionMap.put(deviceId,0.0);
            lastTimestamp = (long) timestamp;
            lastMeasurement = 0;
        }

        long timeDifference = (timestamp - lastTimestamp) / 1000;
        double measurementDifference = measurementValue - lastMeasurement;

        //consider one hour passed after 5 measurements
        double cumulativeValue = hourlyConsumptionMap.get(deviceId) + measurementDifference * timeDifference;

        System.out.println("Cumulative value during the process: "+cumulativeValue);
        NO_OF_MEASUREMENTS++;

        if(NO_OF_MEASUREMENTS >= MEASUREMENTS_PER_HOUR){
            int limitDevice = 0;
            for(MonitoringData a: devices){
                //System.out.println(a.getDevice_id());
                if(a.getDevice_id().equals(UUID.fromString(deviceId))){
                    limitDevice = a.getConsumption();
                    //System.out.println("Device limit is:"+limitDevice);
                }
            }
            //System.out.println("Device limit is:"+limitDevice);
            if(cumulativeValue >= limitDevice){
                System.out.println("Device limit is:"+limitDevice);
                System.out.println("LIIMIT WAS EXCEEDED!!! Device " + deviceId + " Hourly Consumption: " + cumulativeValue);

            }else{
                System.out.println("Hourly consumption did not exceeded time limit!! "+cumulativeValue);
            }

            MonitoringData newData = new MonitoringData(UUID.fromString(deviceId), (int) cumulativeValue);
            monitoringRepository.save(newData);

            NO_OF_MEASUREMENTS = 0;

            hourlyConsumptionMap.put(deviceId,0.0);
            lastMeasurement = measurementValue;
            lastTimestamp = timestamp;
        }else{

            hourlyConsumptionMap.put(deviceId,cumulativeValue);
            lastTimestamp = timestamp;
            lastMeasurement = measurementValue;
        }
    }
}
