package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.entities.MonitoringData;

public class PersonBuilder {

    private PersonBuilder() {
    }

    public static PersonDTO toPersonDTO(MonitoringData person) {
        return new PersonDTO(person.getDevice_id(), person.getConsumption());
    }

    public static PersonDetailsDTO toPersonDetailsDTO(MonitoringData person) {
        return new PersonDetailsDTO(person.getDevice_id(), person.getConsumption());
    }

    public static MonitoringData toEntity(PersonDetailsDTO personDetailsDTO) {
        return new MonitoringData(personDetailsDTO.getId(), personDetailsDTO.getConsumption());
    }
}
