package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.tuc.ds2020.entities.MonitoringData;

import java.util.UUID;

@Repository
public interface MonitoringRepository extends JpaRepository<MonitoringData, UUID> {

    /**
     * Example: JPA generate Query by Field
     */
//    List<Person> findByName(String name);
//
//    /**
//     * Example: Write Custom Query
//     */
//    @Query(value = "SELECT p " +
//            "FROM Person p " +
//            "WHERE p.consumption = :consumption ")
//    Optional<Person> findSeniorsByName(@Param("consumption") String name);

}
