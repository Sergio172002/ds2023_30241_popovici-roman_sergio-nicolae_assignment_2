import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class SensorDataProducer {

    private static final String QUEUE_NAME = "SensorValues";
    private static final String DEVICE_ID = "a3904eb6-7913-401d-8c48-ef3ca4f01b73";
    private static final String RABBITMQ_HOST = "goose-01.rmq2.cloudamqp.com";
    private static final String RABBITMQ_USERNAME = "fqmbnfna";
    private static final String RABBITMQ_PASSWORD = "e7jRtYGIh7sWaSP_LBJBrhqn3-mXMRi9";
    private static final String RABBITMQ_VHOST = "fqmbnfna";


    static List<String[]> readCSV(String filePath) throws IOException, CsvException {
        try (CSVReader reader = new CSVReader(new FileReader(filePath))) {
            return reader.readAll();
        }
    }

    static void produceAndSendData(List<String[]> data) throws IOException, InterruptedException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(RABBITMQ_HOST); // Set RabbitMQ server host
        factory.setUsername(RABBITMQ_USERNAME);
        factory.setPassword(RABBITMQ_PASSWORD);
        factory.setVirtualHost(RABBITMQ_VHOST);

        try (Connection connection = factory.newConnection(); Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);

            for (int i = 1; i < data.size(); i++) { // Skip the first line with column headers
                long timestamp = System.currentTimeMillis();
                double measurementValue = Double.parseDouble(data.get(i)[0]);

                SensorData sensorData = new SensorData(timestamp, DEVICE_ID, measurementValue);
                String json = convertToJSON(sensorData);

                channel.basicPublish("", QUEUE_NAME, null, json.getBytes());
                System.out.println("Sent: " + json);

                TimeUnit.SECONDS.sleep(1); // Wait for 30 seconds before sending the next data point
            }
        }
    }

    private static String convertToJSON(SensorData sensorData) {
        Gson gson = new Gson();
        return gson.toJson(sensorData);
    }

    private static class SensorData {
        private final long timestamp;
        private final String device_id;
        private final double measurement_value;

        public SensorData(long timestamp, String device_id, double measurement_value) {
            this.timestamp = timestamp;
            this.device_id = device_id;
            this.measurement_value = measurement_value;
        }
    }
}
