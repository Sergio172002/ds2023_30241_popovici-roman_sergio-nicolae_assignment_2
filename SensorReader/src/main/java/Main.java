import com.opencsv.exceptions.CsvException;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class Main {
    public static void main(String[] args) {
        try {
            List<String[]> data =   SensorDataProducer.readCSV("C:\\Users\\Popovici Sergio\\Desktop\\subiecte simulare\\sensor.csv");
            SensorDataProducer.produceAndSendData(data);
        } catch (IOException | CsvException | InterruptedException | TimeoutException e) {
            e.printStackTrace();
        }
    }
}
