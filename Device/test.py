def auction_system(*args):
    initial_price = int(args[0])
    bids = args[1:]

    current_price = initial_price
    highest_bidder = None
    highest_bid = 0

    for i in range(0, len(bids), 2):
        bidder = bids[i]
        bid_amount = int(bids[i + 1])

        if bid_amount > highest_bid:
            highest_bid = bid_amount
            highest_bidder = bidder
            current_price = max(current_price + 1, bid_amount + 1)

    return highest_bidder, current_price

# Example usage
input_data = "1,A,5,B,10,A,8,A,17,B,17"
input_values = input_data.split(',')
result = auction_system(*input_values)
print(result)
