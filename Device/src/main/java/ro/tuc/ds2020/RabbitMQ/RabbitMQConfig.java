package ro.tuc.ds2020.RabbitMQ;

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        return new RabbitTemplate(connectionFactory);
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setHost("cow-01.rmq2.cloudamqp.com");
        connectionFactory.setPort(5672);
        connectionFactory.setUsername("vxcoohzc");
        connectionFactory.setPassword("WnfSe9y-b52vGBw6HyUeBogPADnGK1Qa");
        connectionFactory.setVirtualHost("vxcoohzc");
        return connectionFactory;
    }
}
