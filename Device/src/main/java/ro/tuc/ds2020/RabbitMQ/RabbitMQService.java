package ro.tuc.ds2020.RabbitMQ;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeoutException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class RabbitMQService {

    private final RabbitTemplate rabbitTemplate;
    private final ConnectionFactory connectionFactory;


    @Autowired
    public RabbitMQService(RabbitTemplate rabbitTemplate, ConnectionFactory connectionFactory) {
        this.rabbitTemplate = rabbitTemplate;
        this.connectionFactory = connectionFactory;
    }

    private static String convertToJSON(String sensorData) {
        Gson gson = new Gson();
        return gson.toJson(sensorData);
    }

    public void sendMessageToQueue(UUID device_id, Integer hour_consumption) throws IOException, TimeoutException {

        com.rabbitmq.client.ConnectionFactory factory = new com.rabbitmq.client.ConnectionFactory();
        factory.setHost("cow-01.rmq2.cloudamqp.com");
        factory.setPort(5672);
        factory.setUsername("vxcoohzc");
        factory.setPassword("WnfSe9y-b52vGBw6HyUeBogPADnGK1Qa");
        factory.setVirtualHost("vxcoohzc");

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare("DevicesQueue", false, false, false, null);


        try {

            // Message to be sent to the RabbitMQ queue
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(Map.of(
                    "device_id", device_id.toString(),
                    "hour_consumption", hour_consumption
            ));

            channel.basicPublish("", "DevicesQueue", null, json.getBytes());
            System.out.println("Message sent to RabbitMQ queue: " + json);
        } catch (Exception e) {
            // Handle exceptions appropriately (e.g., log or throw)
            e.printStackTrace();
        }
    }
}
