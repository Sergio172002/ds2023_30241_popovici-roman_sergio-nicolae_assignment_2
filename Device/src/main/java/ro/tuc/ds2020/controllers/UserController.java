package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.dtos.UserDetailsDTO;
import ro.tuc.ds2020.dtos.validators.JwtTokenValidator;
import ro.tuc.ds2020.entities.RelationshipRequest;
import ro.tuc.ds2020.services.UserService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/user")
public class UserController {

    private final UserService userService;
    private JwtTokenValidator jwtTokenUtil;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping()
    public ResponseEntity<List<UserDTO>> getUsers() {
        List<UserDTO> dtos = userService.findUsers();
        for (UserDTO dto : dtos) {
            Link deviceLink = linkTo(methodOn(UserController.class)
                    .getUser(dto.getId())).withRel("userDetails");
            dto.add(deviceLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }


    @PostMapping()
    public ResponseEntity<UUID> insertDevice(@Valid @RequestBody UserDetailsDTO userDTO) {
        System.out.println("Uuid that i sent"+userDTO.getId());
        UUID userID = userService.insert(userDTO);
        return new ResponseEntity<>(userID, HttpStatus.CREATED);
    }

    @PostMapping("/create")
    public ResponseEntity<UUID> createRelation(@RequestBody RelationshipRequest req, @RequestHeader(value = "Authorization", required = false) String authorizationHeader) {

        // Check if the authorization header is present
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); // Return 401 Unauthorized if the header is not present or does not start with "Bearer "
        }

        String token = authorizationHeader.substring(7);
        System.out.println(token);

        if (!jwtTokenUtil.isTokenValid(token)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); // Return 401 Unauthorized if the token is invalid
        }

        UUID userID = userService.createRel(req.getParentId(), req.getChildId());
        return new ResponseEntity<>(userID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<UserDetailsDTO> getUser(@PathVariable("id") UUID userId) {
        UserDetailsDTO dto = userService.findUserById(userId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<UUID> deleteUser(@PathVariable("id") UUID userId){

        UUID message = userService.deleteById(userId);
        return new ResponseEntity<>(message, HttpStatus.ACCEPTED);
    }
//
//    @PutMapping(value = "/{id}")
//    public ResponseEntity<UUID> updateAtUuid(@PathVariable("id") UUID personId, @Valid @RequestBody UpdateDeviceDTO updateValueDTO){
//        int newValue = updateValueDTO.getUpdatedValue();
//        System.out.println(newValue+" "+personId);
//        UUID message = deviceService.updateAtId(personId,newValue);
//        return new ResponseEntity<>(message,HttpStatus.ACCEPTED);
//    }
}
