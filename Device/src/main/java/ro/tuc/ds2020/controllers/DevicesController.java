package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.RabbitMQ.RabbitMQService;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.DeviceDetailsDTO;
import ro.tuc.ds2020.dtos.UpdateDeviceDTO;
import ro.tuc.ds2020.dtos.validators.JwtTokenValidator;
import ro.tuc.ds2020.services.DeviceService;

import javax.validation.Valid;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/device")
public class DevicesController {

    private final DeviceService deviceService;
    private final RabbitMQService rabbitMQService;
    private JwtTokenValidator jwtTokenUtil;

    @Autowired
    public DevicesController(DeviceService deviceService, RabbitMQService rabbitMQService) {
        this.deviceService = deviceService;
        this.rabbitMQService = rabbitMQService;
    }

    @GetMapping()
    public ResponseEntity<List<DeviceDTO>> getDevices(@RequestHeader(value = "Authorization", required = false) String authorizationHeader) {

        // Check if the authorization header is present
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); // Return 401 Unauthorized if the header is not present or does not start with "Bearer "
        }

        String token = authorizationHeader.substring(7);
        System.out.println(token);

        if (!jwtTokenUtil.isTokenValid(token)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); // Return 401 Unauthorized if the token is invalid
        }

        List<DeviceDTO> dtos = deviceService.findDevices();
        for (DeviceDTO dto : dtos) {
            Link deviceLink = linkTo(methodOn(DevicesController.class)
                    .getDevice(dto.getId())).withRel("deviceDetails");
            dto.add(deviceLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/free")
    public ResponseEntity<List<DeviceDTO>> getDevicesWithNoUser(@RequestHeader(value = "Authorization", required = false) String authorizationHeader) {

        // Check if the authorization header is present
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); // Return 401 Unauthorized if the header is not present or does not start with "Bearer "
        }

        String token = authorizationHeader.substring(7);
        System.out.println(token);

        if (!jwtTokenUtil.isTokenValid(token)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); // Return 401 Unauthorized if the token is invalid
        }

        List<DeviceDTO> dtos = deviceService.findDevicesWithNoUser();
        for (DeviceDTO dto : dtos) {
            Link deviceLink = linkTo(methodOn(DevicesController.class)
                    .getDevice(dto.getId())).withRel("deviceDetails");
            dto.add(deviceLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/pairs")
    public ResponseEntity<List<Map<String, String>>> getDevicesUserPairs(@RequestHeader(value = "Authorization", required = false) String authorizationHeader) {

        // Check if the authorization header is present
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); // Return 401 Unauthorized if the header is not present or does not start with "Bearer "
        }

        String token = authorizationHeader.substring(7);
        System.out.println(token);

        if (!jwtTokenUtil.isTokenValid(token)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); // Return 401 Unauthorized if the token is invalid
        }

        List<Object[]> userDevicePairs = deviceService.findUserDevicesPairs();
        List<Map<String, String>> dtos = userDevicePairs
                .stream()
                .map(pair -> {
                    Map<String, String> pairMap = new HashMap<>();
                    pairMap.put("username", (String) pair[0]);
                    pairMap.put("description", (String) pair[1]);
                    return pairMap;
                })
                .collect(Collectors.toList());

        for (Map<String, String> dto : dtos) {
            Link deviceLink = linkTo(methodOn(DevicesController.class)
                    .getDeviceDetails(dto.get("username"), dto.get("description"))).withRel("deviceDetails");
            dto.put("deviceLink", deviceLink.getHref());
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/{username}/{description}")
    public ResponseEntity<String> getDeviceDetails(
            @PathVariable String username,
            @PathVariable String description
    ) {
        // Your logic to retrieve device details based on username and description
        String deviceDetails = "Device details for " + username + " - " + description;

        return new ResponseEntity<>(deviceDetails, HttpStatus.OK);
    }



    @DeleteMapping(value = "/{id}")
    public ResponseEntity<UUID> deleteDevice(@PathVariable("id") UUID deviceId, @RequestHeader(value = "Authorization", required = false) String authorizationHeader){

        // Check if the authorization header is present
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); // Return 401 Unauthorized if the header is not present or does not start with "Bearer "
        }

        String token = authorizationHeader.substring(7);
        System.out.println(token);

        if (!jwtTokenUtil.isTokenValid(token)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); // Return 401 Unauthorized if the token is invalid
        }

        UUID message = deviceService.deleteById(deviceId);
        return new ResponseEntity<>(message, HttpStatus.ACCEPTED);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertDevice(@Valid @RequestBody DeviceDetailsDTO deviceDTO) throws IOException, TimeoutException {
        UUID deviceID = deviceService.insert(deviceDTO);



        rabbitMQService.sendMessageToQueue(deviceID,deviceDTO.getHourConsumption());

        return new ResponseEntity<>(deviceID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<DeviceDetailsDTO> getDevice(@PathVariable("id") UUID deviceId) {
        DeviceDetailsDTO dto = deviceService.findDeviceById(deviceId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "/per_user/{id}")
    public ResponseEntity<List<DeviceDTO>> getDeviceForGiven(@PathVariable("id") UUID deviceId) {
        List<DeviceDTO> dtos = deviceService.findDevicesForAnUser(deviceId);
        for (DeviceDTO dto : dtos) {
            Link deviceLink = linkTo(methodOn(DevicesController.class)
                    .getDevice(dto.getId())).withRel("deviceDetails");
            dto.add(deviceLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<UUID> updateAtUuid(@PathVariable("id") UUID personId, @Valid @RequestBody UpdateDeviceDTO updateValueDTO){
        int newValue = updateValueDTO.getUpdatedValue();
        System.out.println(newValue+" "+personId);
        UUID message = deviceService.updateAtId(personId,newValue);
        return new ResponseEntity<>(message,HttpStatus.ACCEPTED);
    }
}
