package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.UserID;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<UserID, UUID> {

    /**
     * Example: JPA generate Query by Field
     */
    List<UserID> findByUsername(String username);

    @Modifying
    @Query("DELETE FROM UserID u WHERE u.username = :username")
    void deleteByUsername(@Param("username") String username);


    /**
     * Example: Write Custom Query
     */
//    @Query(value = "SELECT p " +
//            "FROM Device p " +
//            "WHERE p.description = :description " +
//            "AND p.hourConsumption >= 60  ")
//    Optional<Device> findSeniorsByName(@Param("description") String description);
//
//    @Modifying
//    @Query("UPDATE Device p SET p.hourConsumption = :newHourConsumption WHERE p.id = :uuid")
//    int updateNameByUuid(@Param("uuid") UUID uuid, @Param("newHourConsumption") Integer newHourConsumption);
}
