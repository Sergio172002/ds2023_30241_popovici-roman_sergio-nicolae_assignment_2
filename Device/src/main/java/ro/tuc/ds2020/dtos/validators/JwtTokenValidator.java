package ro.tuc.ds2020.dtos.validators;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.ExpiredJwtException;
import java.util.Date;

public class JwtTokenValidator {

    private static final String SECRET_KEY = "aVeryLongStringThatContainsAMixOfUpperAndLowerCaseLettersNumbers1234567890AndSymbolsAABBCCDDEEFFGGHHIIJJKKLL"; // Ensure this is the same key used to generate the token

    // Method to check if the token is expired and validate the secret key
    public static boolean isTokenValid(String token) {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(SECRET_KEY)
                    .parseClaimsJws(token)
                    .getBody();

            // Check if the token is expired
            return !claims.getExpiration().before(new Date());
        } catch (ExpiredJwtException e) {
            // Token is expired
            System.out.println("Token is expired!");
            return false;
        } catch (SignatureException e) {
            // Signature does not match the secret key
            System.out.println("Signature does not match the secret key");
            return false;
        }
        // Add catches for other relevant exceptions as necessary
    }
}
