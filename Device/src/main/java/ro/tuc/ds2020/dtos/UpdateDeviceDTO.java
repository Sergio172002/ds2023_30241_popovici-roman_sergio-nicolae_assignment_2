package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;

public class UpdateDeviceDTO extends RepresentationModel<DeviceDTO> {

    private int updatedValue;

    // public UpdatePersonDTO(int updatedValue) {
    //    this.updatedValue = updatedValue;
    //}

    public int getUpdatedValue() {
        return updatedValue;
    }

    public void setUpdatedValue(int updatedValue) {
        this.updatedValue = updatedValue;
    }
}
