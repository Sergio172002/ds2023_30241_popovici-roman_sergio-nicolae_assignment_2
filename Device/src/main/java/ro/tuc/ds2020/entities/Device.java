package ro.tuc.ds2020.entities;

import jakarta.persistence.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import java.io.Serializable;
import java.util.UUID;

@Entity
public class Device implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    //@Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "hourConsumption", nullable = false)
    private int hourConsumption;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserID user;

    public Device(UUID id, String description, String address, int hourConsumption,UserID user) {
        this.id = id;
        this.description = description;
        this.address = address;
        this.hourConsumption = hourConsumption;
        this.user = user;
    }

    public Device() {
    }

    public Device(String description, String address, int hourConsumption ) {
        this.description = description;
        this.address = address;
        this.hourConsumption = hourConsumption;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getHourConsumption() {
        return hourConsumption;
    }

    public void setHourConsumption(int hourConsumption) {
        this.hourConsumption = hourConsumption;
    }
}
