package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.DeviceDetailsDTO;
import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.dtos.UserDetailsDTO;
import ro.tuc.ds2020.dtos.builders.UserBuilder;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.UserID;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final UserRepository userRepository;
    private final DeviceRepository deviceRepository;

    @Autowired
    public UserService(UserRepository userRepository, DeviceRepository deviceRepository) {
        this.userRepository = userRepository;
        this.deviceRepository = deviceRepository;
    }

    @Autowired
    public DeviceService deviceService;


    public List<UserDTO> findUsers() {
        List<UserID> deviceList = userRepository.findAll();

        for (UserID user : deviceList) {
            System.out.println(user.getId() + user.getName());
        }
        return deviceList.stream()
                .map(UserBuilder::toUserDTO)
                .collect(Collectors.toList());
    }

    public UserDetailsDTO findUserById(UUID id) {
        Optional<UserID> userOptional = userRepository.findById(id);
        if (!userOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Device.class.getSimpleName() + " with id: " + id);
        }
        return UserBuilder.toUserDetailsDTO(userOptional.get());
    }

    @Transactional
    public UUID deleteById( UUID id) {
        Optional<UserID> prosumerOptional = userRepository.findById(id);

        if (!prosumerOptional.isPresent()) {
            LOGGER.error("User with id {} was not found in db", id);
            throw new ResourceNotFoundException(Device.class.getSimpleName() + " with id: " + id);
        }else{
            LOGGER.error("User with id {} was deleted from the db", id);
            userRepository.deleteById(id);
        }
        return id;
    }

//    @Transactional
//    public UUID updateAtId(UUID id, int age) {
//        Optional<Device> prosumerOptional = deviceRepository.findById(id);
//        if (!prosumerOptional.isPresent()) {
//            LOGGER.error("Device with id {} was not found in db", id);
//            throw new ResourceNotFoundException(Device.class.getSimpleName() + " with id: " + id);
//        }else{
//            LOGGER.error("Person with id {} was updated", id);
//            deviceRepository.updateNameByUuid(id, age);
//        }
//
//        // Create a simple JSON object with a key-value pair
//        return id;
//    }

    public UUID insert(UserDetailsDTO userDTO) {
        UserID user =UserBuilder.toEntity(userDTO);
        System.out.println("Id that is before the save: "+ userDTO.getId());
        user.setId(userDTO.getId());
        user = userRepository.save(user);
        LOGGER.debug("Device with id {} was inserted in db", user.getId());
        return user.getId();
    }

    @Transactional
    public UUID createRel(UUID id1, UUID id2) {

        UserDetailsDTO user = this.findUserById(id1);
        UserID completeUser = new UserID(user.getId(), user.getUsername());

        DeviceDetailsDTO device = deviceService.findDeviceById(id2);
        Device completeDevice = new Device(device.getId(),device.getDescription(), device.getAddress(),device.getHourConsumption(),completeUser);

        if(user!=null && device !=null){
            completeUser.getChildren().add(completeDevice);
            userRepository.save(completeUser);
            deviceRepository.save(completeDevice);
        }

        return completeUser.getId();
    }

}
