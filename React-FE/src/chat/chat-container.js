import React, { useState, useEffect, useRef } from 'react';
import { Stomp } from '@stomp/stompjs';
import SockJS from 'sockjs-client';
//import Notification from '../person/components/notification';




const ChatContainer = () => {
    const [messages, setMessages] = useState([]);
    const [inputMessage, setInputMessage] = useState('');
    const stompClientRef = useRef(null);
    const serverUrl = 'http://localhost:8085';
    const queryParams = new URLSearchParams(window.location.search);
    const sender = queryParams.get('sender');
    const recipient = queryParams.get('recipient');
    const [isTyping, setIsTyping] = useState(false);
    const [recipientIsTyping, setRecipientIsTyping] = useState(false);
    const [recipientReadMessages, setRecipientReadMessages] = useState([]);
    const [notificationMessage, setNotificationMessage] = useState('');

    const senderMessages = messages.filter((msg) => msg.sender === sender);
    const recipientMessages = messages.filter((msg) => msg.sender === recipient && msg.recipient === sender);

    useEffect(() => {
        const socket = new SockJS(`${serverUrl}/ws`, null, {
            transports: ['websocket', 'xhr-polling', 'iframe-eventsource'],
            origins: 'http://localhost:3000',
        });

        const stomp = Stomp.over(socket);

        const connectWebSocket = () => {
            stomp.connect({}, (frame) => {
                console.log('Connected to WebSocket ', frame);
                if (stomp.connected) {
                    console.log('Is connected:', stomp.connected);
                    stompClientRef.current = stomp;
                } else {
                    console.log('Is connected null');
                }

                stomp.subscribe('/topic/chat', (message) => {
                    const newMessages = [...messages, JSON.parse(message.body)];
                    console.log('!!New messages: ', newMessages);
                    setMessages(newMessages);
                    console.log('Received message on the frontend:', JSON.parse(message.body));
                });


            });
        };

        const disconnectWebSocket = () => {
            if (stomp.connected) {
                stomp.disconnect(() => {
                    console.log('Disconnected from WebSocket');
                });
            }
            stompClientRef.current = null;
        };

        connectWebSocket();

        return () => {
            disconnectWebSocket();
        };
    }, [serverUrl, messages, sender, recipient]);

    const sendMessage = () => {
        console.log('### inceput de send message');
        console.log('### Is stompClientRef.current:', stompClientRef.current);

        if (stompClientRef.current && stompClientRef.current.connected && inputMessage.trim() !== '') {
            console.log('### Is WebSocket connected:', stompClientRef.current.connected);
            console.log('### is input message != ', inputMessage.trim());

            console.log('### intra in if de send message');
            const newMessage = {
                sender: sender,
                recipient: recipient,
                content: inputMessage,
                timestamp: new Date().toISOString(),
            };

            stompClientRef.current.send('/chat/send', {}, JSON.stringify(newMessage));
            setInputMessage('');

        } else {
            console.log('### not sending message');
        }
    };

    const handleInputChange = (e) => {
        setInputMessage(e.target.value);
        setIsTyping(true);

        if (stompClientRef.current && stompClientRef.current.connected) {
            const typingNotification = {
                sender: sender,
                recipient: recipient,
                typing: true,
            };

            stompClientRef.current.send('/chat/typing', {}, JSON.stringify(typingNotification));
        }
    };

    return (
        <div>

            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <div style={{ flex: 1, textAlign: 'right', marginRight: '10px' }}>
                    {senderMessages.map((msg, index) => (
                        <div key={index}>
                            <strong>{msg.sender}:</strong> {msg.content}
                        </div>
                    ))}
                </div>

                <div style={{ flex: 1, textAlign: 'left', marginLeft: '10px' }}>
                    {recipientMessages.map((msg, index) => (
                        <div key={index}>
                            <strong>{msg.sender}:</strong> {msg.content}
                        </div>
                    ))}
                </div>
            </div>

            <div>
                <input type="text" value={inputMessage} onChange={handleInputChange} />
                <button onClick={sendMessage}>Send</button>
            </div>
        </div>
    );
};

export default ChatContainer;