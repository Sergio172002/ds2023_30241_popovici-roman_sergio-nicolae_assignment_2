import {HOST, HOST_DEVICES} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";



const endpoint = {
    person: '/person',
    user: '/user'
};

function getPersons(callback) {

    const storedData = JSON.parse(localStorage.getItem("user"));
    const token = storedData ? storedData.token : null;
    console.log(token)

    const headers = new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token  // Include the token in the Authorization header
    });

    let request = new Request(HOST.backend_api + endpoint.person, {
        method: 'GET',
        headers: headers
    });
    console.log("URL: "+request.url);
    console.log("BODY: "+request.body)
    console.log("HEADERS: " + headers.get('Authorization'));
    RestApiClient.performRequest(request, callback);
}

function getPersonById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.person + params.id, {
       method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postPerson(user, callback){

    // const storedData = JSON.parse(localStorage.getItem("user"));
    // const token = storedData ? storedData.token : null;
    // console.log(token)

    const headers = new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
       // 'Authorization': 'Bearer ' + token  // Include the token in the Authorization header
    });

    let request = new Request(HOST.backend_api + endpoint.person , {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);

}

function postUserToDevicesDB(user, callback) {
    let request = new Request("http://localhost:8090/user", {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);

}


function putPerson(updatedValue, id, callback){

    const storedData = JSON.parse(localStorage.getItem("user"));
    const token = storedData ? storedData.token : null;
    console.log(token)

    const headers = new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token  // Include the token in the Authorization header
    });

    let request = new Request(HOST.backend_api + endpoint.person + "/"+ id , {
        method: 'PUT',
        headers : headers,
        body: JSON.stringify(updatedValue)
    });

    console.log(JSON.stringify(updatedValue))
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deletePerson(id, callback){

    const storedData = JSON.parse(localStorage.getItem("user"));
    const token = storedData ? storedData.token : null;
    console.log(token)

    const headers = new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token  // Include the token in the Authorization header
    });

    let request = new Request(HOST.backend_api + endpoint.person + "/"+ id , {
        method: 'DELETE',
        headers : headers
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteUserFromDevicesDB(id, callback){
    let request = new Request(HOST_DEVICES.backend_api + endpoint.user + "/"+ id , {
        method: 'DELETE',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    deleteUserFromDevicesDB,
    postUserToDevicesDB,
    deletePerson,
    getPersons,
    getPersonById,
    postPerson,
    putPerson
};
