import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import PersonForm from "./components/person-form";
import PersonUpdateForm from "./components/person-update-form";
import PersonDeleteForm from "./components/person-delete-form";

import * as API_USERS from "./api/person-api"
import PersonTable from "./components/person-table";



class PersonContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.toggleUpdateModal = this.toggleUpdateModal.bind(this);
        this.toggleDeleteModal = this.toggleDeleteModal.bind(this);
        this.reload = this.reload.bind(this);
        this.reloadUpdate = this.reloadUpdate.bind(this);
        this.reloadDelete = this.reloadDelete.bind(this);
        this.state = {
            isDeleteModalOpen: false,
            isUpdateModalOpen: false,
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchPersons();
    }

    fetchPersons() {
        return API_USERS.getPersons((result, status, err) => {
            if (result !== null && (status === 200 || status === 202 )) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }


    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    toggleUpdateModal() {
        this.setState({isUpdateModalOpen: !this.state.isUpdateModalOpen});
    }

    toggleDeleteModal() {
        this.setState({isDeleteModalOpen: !this.state.isDeleteModalOpen});
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchPersons();
    }

    reloadUpdate(){
        this.setState({
            isLoaded: false
        });
        this.toggleUpdateModal();
        this.fetchPersons();
    }

    reloadDelete(){
        this.setState({
            isLoaded: false
        });
        this.toggleDeleteModal();
        this.fetchPersons();
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Person Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '1', offset: 2}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Person </Button>
                        </Col>

                        <Col sm={{size: '1', offset: 1}}>
                            <Button color="primary" onClick={this.toggleUpdateModal}>Update Person </Button>
                        </Col>

                        <Col sm={{size: '1', offset: 1}}>
                            <Button color="primary" onClick={this.toggleDeleteModal}>Delete Person </Button>
                        </Col>

                    </Row>

                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <PersonTable tableData = {this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                                            errorStatus={this.state.errorStatus}
                                                            error={this.state.error}
                                                        />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Person: </ModalHeader>
                    <ModalBody>
                        <PersonForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.isUpdateModalOpen} toggle={this.toggleUpdateModal}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleUpdateModal}> Update Person: </ModalHeader>
                    <ModalBody>
                        <PersonUpdateForm reloadHandler={this.reloadUpdate}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.isDeleteModalOpen} toggle={this.toggleDeleteModal}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleDeleteModal}> Delete Person: </ModalHeader>
                    <ModalBody>
                        <PersonDeleteForm reloadHandler={this.reloadDelete}/>
                    </ModalBody>
                </Modal>

            </div>
        )

    }
}


export default PersonContainer;
