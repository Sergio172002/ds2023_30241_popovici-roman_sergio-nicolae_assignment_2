import {HOST_DEVICES} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";



const endpoint = {
    person: '/device'
};

function getDevices(callback) {

    const storedData = JSON.parse(localStorage.getItem("user"));
    const token = storedData ? storedData.token : null;
    console.log(token)

    const headers = new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token  // Include the token in the Authorization header
    });

    let request = new Request(HOST_DEVICES.backend_api + endpoint.person, {
        method: 'GET',
        headers: headers
    });
    console.log(request.url);

    RestApiClient.performRequest(request, callback);
}

function getDeviceById(params, callback){
    let request = new Request(HOST_DEVICES.backend_api + endpoint.person + params.id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postDevice(user, callback){

    const storedData = JSON.parse(localStorage.getItem("user"));
    const token = storedData ? storedData.token : null;
    console.log(token)

    const headers = new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token  // Include the token in the Authorization header
    });

    let request = new Request(HOST_DEVICES.backend_api + endpoint.person , {
        method: 'POST',
        headers : headers,
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);
    console.log(user);

    RestApiClient.performRequest(request, callback);
}

function putDevice(updatedValue, id, callback){

    const storedData = JSON.parse(localStorage.getItem("user"));
    const token = storedData ? storedData.token : null;
    console.log(token)

    const headers = new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token  // Include the token in the Authorization header
    });

    let request = new Request(HOST_DEVICES.backend_api + endpoint.person + "/"+ id , {
        method: 'PUT',
        headers : headers,
        body: JSON.stringify(updatedValue)
    });

    console.log(JSON.stringify(updatedValue))
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteDevice(id, callback){

    const storedData = JSON.parse(localStorage.getItem("user"));
    const token = storedData ? storedData.token : null;
    console.log(token)

    const headers = new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token  // Include the token in the Authorization header
    });

    let request = new Request(HOST_DEVICES.backend_api + endpoint.person + "/"+ id , {
        method: 'DELETE',
        headers : headers
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    deleteDevice,
    getDevices,
    getDeviceById,
    postDevice,
    putDevice
};
