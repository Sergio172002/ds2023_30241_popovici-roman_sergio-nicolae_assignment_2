import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Card,
    CardHeader,
    Col,
    Row
} from 'reactstrap';

import Button from "react-bootstrap/Button";
import SelectUser from "../ConnectTab/components/select-user";
import {getCurrentUser} from "../login/api/login-api";

class ConnectionContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedDevice: null,
            selectedUser: null,
            selected: false,
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }


    handleUserSelect = (selectedUser) => {
        this.setState({ selectedUser });
        console.log(selectedUser);
    };

    fetchAssociation() {
        const { selectedUser } = this.state;
        const requestBody = {
            parentId: selectedUser.value,
        };
        console.log(requestBody);

        const user = getCurrentUser();
        const myusername = user.personDetails.username;

        window.location.href = '/chat?sender=' + myusername + '&recipient=' + selectedUser.label;
    }


    componentDidMount() {

    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    handleSubmit = () => {
        this.fetchAssociation();
        this.reload();
    }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();

    }

    render() {
        return (
            <div style={{ marginTop: '10px'} }>
                <CardHeader>
                    <strong>Associations Management</strong>
                </CardHeader>
                <Card style={{ marginTop: '30px'} } >
                    <Row>
                        <Col sm="6">
                            <SelectUser onUserSelect={this.handleUserSelect} />
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: '30px'} }>
                        <Col sm={{ size: '4', offset: 4 }}>
                            <div className="d-flex justify-content-center">
                                <Button type="submit" onClick={this.handleSubmit}>Submit</Button>
                            </div>
                        </Col>
                    </Row>
                </Card>
                <Card>

                </Card>
            </div>
        );
    }
}

export default ConnectionContainer;
