import {HOST_DEVICES, HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";



const endpoint = {
    device: '/device',
    user: '/user',
    free: '/free',
    create: '/create',
    pairs:'/pairs',
    person: '/person'
};

function getUsers(callback) {

    const storedData = JSON.parse(localStorage.getItem("user"));
    const token = storedData ? storedData.token : null;
    console.log(token)

    const headers = new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token  // Include the token in the Authorization header
    });

    let request = new Request(HOST.backend_api + endpoint.person, {
        method: 'GET',
        headers: headers
    });
    console.log("URL: "+request.url);
    console.log("BODY: "+request.body)
    console.log("HEADERS: " + headers.get('Authorization'));
    RestApiClient.performRequest(request, callback);
}


export{
    getUsers,
};