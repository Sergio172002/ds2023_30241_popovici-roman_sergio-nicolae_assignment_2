import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Card,
    CardHeader,
    Col,
    Row
} from 'reactstrap';

import UserTable from "./components/user-table";
import {getCurrentUser} from "../login/api/login-api";
import * as API from "../user/api/user-api.js"

class UserContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selected: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }


    fetchPairs() {
        const user = getCurrentUser();
        const id = user.personDetails.id
        return API.getDevicesOfLoggedUser(id,(result, status, err) => {
            if (result !== null && (status === 200 || status === 202 )) {
                console.log(result);
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    componentDidMount() {
        this.fetchPairs();
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    // handleSubmit = () => {
    //     this.fetchAssociation();
    //     this.reload();
    // }

    // reload() {
    //     this.setState({
    //         isLoaded: false
    //     });
    //     this.toggleForm();
    //     this.fetchPairs();
    // }

    render() {
        return (
            <div style={{ marginTop: '10px'} }>
                <CardHeader>
                    <strong>Devices associated with you:</strong>
                </CardHeader>
                <Card style={{ marginTop: '30px'} } >
                    <Row  style={{ marginTop: '100px'}}>
                        <Col sm={{size: '8', offset: 2}}>
                            {this.state.isLoaded && <UserTable tableData = {this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>
                <Card>

                </Card>
            </div>
        );
    }
}

export default UserContainer;
