import {HOST_DEVICES, HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";



const endpoint = {
    device: '/device',
    user: '/user',
    free: '/free',
    create: '/create',
    pairs:'/pairs',
    per_user: '/per_user'
};

function getUsers(callback) {

    const storedData = JSON.parse(localStorage.getItem("user"));
    const token = storedData ? storedData.token : null;

    const headers = new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token  // Include the token in the Authorization header
    });


    let request = new Request(HOST_DEVICES.backend_api + endpoint.user, {
        method: 'GET',
        //headers: headers
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getDevicesOfLoggedUser(id, callback){
    let request = new Request(HOST_DEVICES.backend_api + endpoint.device + endpoint .per_user+ "/"+ id , {
        method: 'GET',
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


export{
    getUsers,
    getDevicesOfLoggedUser
};