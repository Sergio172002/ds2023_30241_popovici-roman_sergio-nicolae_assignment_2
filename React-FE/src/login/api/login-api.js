import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    login: '/user/login'
};

function login(user,callback) {
    let request = new Request(HOST.backend_api + endpoint.login, {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });
    console.log(request.url);
    console.log(user);

    RestApiClient.performLoginRequest(request, callback);
}

function getCurrentUser(){
    if(localStorage.getItem("user"))
        return JSON.parse(localStorage.getItem("user"));
    else return undefined;
}

function logout(){
    localStorage.removeItem("user");
}

export {
    login,
    getCurrentUser,
    logout
}