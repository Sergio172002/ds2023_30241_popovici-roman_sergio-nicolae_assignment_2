import React from 'react';
import {
    Button,
    Card,
    CardHeader,
    Col, Input, Label,
    Modal,
    ModalBody,
    ModalHeader,
    Row,FormGroup
} from 'reactstrap';
import validate from "../person/components/validators/person-validators.js";
import {getCurrentUser, login} from "./api/login-api"
import {toast} from "react-toastify";
import {Redirect} from "react-router-dom";


toast.configure()

class LoginContainer extends React.Component {

    constructor(props) {
        super(props);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {
            redirect: null,
            errorStatus:0,
            error: null,

            formIsValid:false,
            formControls: {
                username:{
                    value: '',
                    placeholder: "Username",
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 1,
                        isRequired:true
                    }
                },
                password:{
                    value: '',
                    placeholder: "Password",
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength:1,
                        isRequired: true
                    }
                }
            }
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    handleChange = event =>{
        const name = event.target.name;
        const value = event.target.value;

        const updateControls = this.state.formControls;
        const updatedFormElem = updateControls[name];

        updatedFormElem.value=value;
        updatedFormElem.touched = true;
        updatedFormElem.valid = validate(value,updatedFormElem.validationRules);
        updateControls[name]=updatedFormElem;

        let formIsValid = true;
        for (let updatedFormElementName in updateControls) {
            formIsValid = updateControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updateControls,
            formIsValid: formIsValid
        });


    }

    handleLogin(e) {
        let user = {
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value
        };

        console.log(user);
        login(user,(result,status,error)=>{
            if(result!==null && status===200){
                const user=getCurrentUser();
                console.log(user);
                this.setState({redirect: "/"});
                window.location.reload();
            }
            else {
                toast("Invalid Credentials");
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        })
    }


    render() {
        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }

        return (
            <div>

                <FormGroup id='username'>
                    <Label for='usernameField'> Name: </Label>
                    <Input name='username' id='usernameField' placeholder={this.state.formControls.username.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.username.value}
                           touched={this.state.formControls.username.touched? 1 : 0}
                           valid={this.state.formControls.username.valid}
                           required
                    />
                    {this.state.formControls.username.touched && !this.state.formControls.username.valid &&
                        <div className={"error-message"}> * Username must have a valid format</div>}
                </FormGroup>

                <FormGroup id='password' >
                    <Label for='passwordField'> Password: </Label>
                    <Input type='password' name='password' id='passwordField' placeholder={this.state.formControls.password.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.password.value}
                           touched={this.state.formControls.password.touched? 1 : 0}
                           valid={this.state.formControls.password.valid}
                           required
                    />
                    {this.state.formControls.password.touched && !this.state.formControls.password.valid &&
                        <div className={"error-message"}> * Password must have a valid format</div>}
                </FormGroup>
                <Button type={"login"} disabled={!this.state.formIsValid} onClick={this.handleLogin}>  Login </Button>
            </div>
        )
    }

}
export default LoginContainer;