import React, {Component} from 'react';
import Select from 'react-select';
import * as API_USERS from '../../associations/api/associations-api.js';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message"; // Import your API functions

class SelectDevice extends Component {
    constructor(props) {
        super(props);

        this.state = {
            deviceOptions: [],
            selectedDevice: null,
            isLoaded: false,
            errorStatus: null,
            error: null,
        };
    }

    processResult = (result) => {
        // Process the result data here as needed
        return result.map(deviceName => ({
            value: deviceName,
            label: deviceName,
        }));
    };

    fetchDevices() {
        return API_USERS.getDevicesWithNoUser((result, status, err) => {
            if (result !== null && (status === 200 || status === 202)) {
                const processedData = this.processResult(result);
                console.log(processedData)
                this.setState({
                    deviceOptions: processedData,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    componentDidMount() {
        // Fetch device data and populate the options
        this.fetchDevices();
    }

    handleDeviceSelect = selectedDevice => {
        this.setState({ selectedDevice });
        this.props.onDeviceSelect(selectedDevice);
    };

    render() {
        return (
            <div>
                <Select
                    value={this.state.selectedDevice}
                    onChange={this.handleDeviceSelect}
                    options={this.state.deviceOptions}
                    isSearchable={true}
                    placeholder="Select a device"
                />
                {this.state.errorStatus && (
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} />
                )}
            </div>
        );
    }
}

export default SelectDevice;
