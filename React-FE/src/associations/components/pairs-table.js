// import React from "react";
// import Table from "../../commons/tables/table";
//
//
// const columns = [
//     {
//         Header: 'Description',
//         accessor: 'description',
//     },
//     {
//         Header: 'Username',
//         accessor: 'username',
//     },
// ];
//
// const filters = [
//     {
//         accessor: 'name',
//     }
// ];
//
// class PairsTable extends React.Component {
//
//     constructor(props) {
//         super(props);
//         this.state = {
//             tableData: this.props.tableData
//         };
//     }
//
//     render() {
//         return (
//             <Table
//                 data={this.state.tableData}
//                 columns={columns}
//                 search={filters}
//                 pageSize={4}
//             />
//         )
//     }
// }
//
// export default PairsTable;