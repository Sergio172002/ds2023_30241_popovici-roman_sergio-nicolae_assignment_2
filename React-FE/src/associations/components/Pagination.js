import React from 'react';

const Pagination = ({ dots, index, onChangeIndex }) => {
    return (
        <div style={{ display: 'flex', justifyContent: 'center', marginTop: '20px' }}>
            {Array.from({ length: dots }).map((_, i) => (
                <div
                    key={i}
                    onClick={() => onChangeIndex(i)}
                    style={{
                        height: '10px',
                        width: '10px',
                        margin: '0 5px',
                        borderRadius: '50%',
                        backgroundColor: i === index ? 'black' : 'lightgray',
                        cursor: 'pointer'
                    }}
                />
            ))}
        </div>
    );
};

export default Pagination;
