import {HOST_DEVICES, HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";



const endpoint = {
    device: '/device',
    user: '/user',
    free: '/free',
    create: '/create',
    pairs:'/pairs',
    ble: '/ble',
    unique_device_name: '/unique-device-names'
};

function getUsers(callback) {

    const storedData = JSON.parse(localStorage.getItem("user"));
    const token = storedData ? storedData.token : null;
    console.log(token)

    const headers = new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token  // Include the token in the Authorization header
    });

    let request = new Request(HOST_DEVICES.backend_api + endpoint.user, {
        method: 'GET',
        headers: headers
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getDevicesWithNoUser(callback) {

    const storedData = JSON.parse(localStorage.getItem("user"));
    const token = storedData ? storedData.token : null;
    console.log(token)

    const headers = new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token  // Include the token in the Authorization header
    });

    let request = new Request(HOST.backend_api + endpoint.ble + endpoint.unique_device_name, {
        method: 'GET',
        headers: headers
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getValuesForDevices(value, callback) {

    const storedData = JSON.parse(localStorage.getItem("user"));
    const token = storedData ? storedData.token : null;
    console.log(token)

    const headers = new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token  // Include the token in the Authorization header
    });

    let request = new Request(HOST.backend_api + endpoint.ble + '/' +value, {
        method: 'GET',
        headers: headers
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getDevicesUsersPairs(callback) {

    const storedData = JSON.parse(localStorage.getItem("user"));
    const token = storedData ? storedData.token : null;
    console.log(token)

    const headers = new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token  // Include the token in the Authorization header
    });

    let request = new Request(HOST_DEVICES.backend_api + endpoint.device + endpoint.pairs, {
        method: 'GET',
        headers: headers
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postAssociation(value, callback){

    const storedData = JSON.parse(localStorage.getItem("user"));
    const token = storedData ? storedData.token : null;
    console.log(token)

    const headers = new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token  // Include the token in the Authorization header
    });

    let request = new Request(HOST_DEVICES.backend_api + endpoint.user + endpoint.create , {
        method: 'POST',
        headers : headers,
        body: JSON.stringify(value)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export{
  getUsers,
  getDevicesWithNoUser,
  postAssociation,
  getDevicesUsersPairs,
    getValuesForDevices
};