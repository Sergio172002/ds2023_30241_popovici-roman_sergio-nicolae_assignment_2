import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Card,
    CardHeader,
    Col,
    Row
} from 'reactstrap';
//import SelectUser from "./components/select-user";
import SelectDevice from "./components/select-device";
import * as API_USERS from "../devices/api/device-api";
import * as API_ASSOCIATION from "../associations/api/associations-api";
import Button from "react-bootstrap/Button";
import DeviceTable from "../devices/components/device-table";
//import PairsTable from "./components/pairs-table";

//import * as API_USERS from '../../associations/api/associations-api.js';
import selectDevice from "./components/select-device";
import { Bar, Line } from 'react-chartjs-2'; // Ensure this import is correct
import 'chart.js/auto'; // required for Chart.js 3
import SwipeableViews from 'react-swipeable-views';

import { autoPlay } from 'react-swipeable-views-utils';
import Pagination from "./components/Pagination";

//const AutoPlaySwipeableViews = autoPlay(SwipeableViews, {interval: 100000});


class AssociationsContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedDevice: null,
            dailyStats: {},
            monthlyStats: {},
            progressData: {},
            selected: false,
            isLoaded: false,
            errorStatus: 0,
            error: null,
            index: 0
        };
    }


    // handleUserSelect = (selectedUser) => {
    //     this.setState({ selectedUser });
    //     console.log(selectedUser);
    // };

    handleDeviceSelect = (selectedDevice) => {
        this.setState({ selectedDevice });
        console.log(selectedDevice);
    };

    // fetchAssociation() {
    //     const { selectedUser, selectedDevice } = this.state;
    //     const requestBody = {
    //         parentId: selectedUser.value,
    //         childId: selectedDevice.value
    //     };
    //     console.log(requestBody);
    //     return API_ASSOCIATION.postAssociation(requestBody, (result, status, error) => {
    //         if (result !== null && (status === 200 || status === 201 || status === 202)) {
    //             console.log("Successfully updated device with id: " + result + status + error);
    //             // this.reloadHandler();
    //         } else {
    //             this.setState({
    //                 errorStatus: status,
    //                 error: error
    //             });
    //         }
    //     });
    // }

    // fetchPairs() {
    //     return API_ASSOCIATION.getDevicesUsersPairs((result, status, err) => {
    //         if (result !== null && (status === 200 || status === 202 )) {
    //             this.setState({
    //                 tableData: result,
    //                 isLoaded: true
    //             });
    //         } else {
    //             this.setState(({
    //                 errorStatus: status,
    //                 error: err
    //             }));
    //         }
    //     });
    // }
    // componentDidMount() {
    //     this.fetchPairs();
    // }

    fetchDevices() {

        const { selectedDevice } = this.state;

        const deviceName = selectedDevice.value;

        console.log(deviceName)

        return API_ASSOCIATION.getValuesForDevices(deviceName,(result, status, err) => {
            if (result !== null && (status === 200 || status === 202)) {
                //const processedData = this.processResult(result);
                this.processStatistics(result);
                console.log(result)
                this.setState({
                   // deviceOptions: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

     handleSubmit = () => {
         this.fetchDevices();
         this.reload();
     }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchDevices();
    }

    processStatistics = (data) => {
        const daily = {};
        const monthly = {};
        const progress = {};

        // Find the latest date in the data
        const dates = data.map(record => new Date(record.time));
        const latestDate = new Date(Math.max.apply(null, dates));

        const fifteenDaysAgo = new Date(latestDate);
        fifteenDaysAgo.setDate(latestDate.getDate() - 15);

        const sixMonthsAgo = new Date(latestDate);
        sixMonthsAgo.setMonth(latestDate.getMonth() - 6);

        data.forEach(record => {
            const date = new Date(record.time);
            const day = date.toISOString().split('T')[0];
            const month = `${date.getFullYear()}-${(date.getMonth() + 1).toString().padStart(2, '0')}`;

            if (date >= fifteenDaysAgo) {
                if (!daily[day]) daily[day] = 0;
                daily[day]++;
            }

            if (date >= sixMonthsAgo) {
                if (!monthly[month]) monthly[month] = 0;
                monthly[month]++;
            }

            if (!progress[month]) progress[month] = 0;
            progress[month]++;
        });

        this.setState({ dailyStats: daily, monthlyStats: monthly, progressData: progress });
    };

    generateChartData = (data, label) => {
        return {
            labels: Object.keys(data),
            datasets: [
                {
                    label: label,
                    data: Object.values(data),
                    backgroundColor: 'rgba(75, 192, 192, 0.6)',
                    borderColor: 'rgba(75, 192, 192, 1)',
                    borderWidth: 1,
                },
            ],
        };
    };

    handleChangeIndex = index => {
        this.setState({ index });
    };

    render() {

        const { dailyStats, monthlyStats, progressData, errorStatus, error, isLoaded, index } = this.state;
        return (
            <div style={{ height: '100vh', display: 'flex', flexDirection: 'column', padding: '20px' }}>
                <CardHeader>
                    <strong>Associations Management</strong>
                </CardHeader>
                <Card style={{ marginTop: '20px', padding: '20px', borderRadius: '10px', boxShadow: '0 4px 8px rgba(0,0,0,0.1)', flex: '0 0 auto' }} >
                    <Row>
                        {/*<Col sm="6">*/}
                        {/*    <SelectUser onUserSelect={this.handleUserSelect} />*/}
                        {/*    {this.state.errorStatus > 0 && <APIResponseErrorMessage*/}
                        {/*        errorStatus={this.state.errorStatus}*/}
                        {/*        error={this.state.error}*/}
                        {/*    />}*/}
                        {/*</Col>*/}
                        <Col sm="6">
                            <SelectDevice onDeviceSelect={this.handleDeviceSelect} />
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: '30px'} }>
                        <Col sm={{ size: '4', offset: 4 }}>
                            <div className="d-flex justify-content-center">
                                <Button type="submit" onClick={this.handleSubmit} style={{ borderRadius: '20px', padding: '10px 20px' }}>Submit</Button>
                            </div>
                        </Col>
                    </Row>
                    {/*<Row  style={{ marginTop: '100px'}}>*/}
                    {/*    <Col sm={{size: '8', offset: 2}}>*/}
                    {/*        {this.state.isLoaded && <PairsTable tableData = {this.state.tableData}/>}*/}
                    {/*        {this.state.errorStatus > 0 && <APIResponseErrorMessage*/}
                    {/*            errorStatus={this.state.errorStatus}*/}
                    {/*            error={this.state.error}*/}
                    {/*        />   }*/}
                    {/*    </Col>*/}
                    {/*</Row>*/}
                </Card>
                {isLoaded && (
                    <Card style={{ marginTop: '20px', padding: '20px', borderRadius: '10px', boxShadow: '0 4px 8px rgba(0,0,0,0.1)', flexGrow: 1, display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
                        <SwipeableViews style={{ marginTop: '15px', padding: '15px'}} index={index} onChangeIndex={this.handleChangeIndex} >
                            <div style={{ padding: '20px' }}>
                                <h2 style={{ textAlign: 'center' }} >Daily Misuse Statistics</h2>
                                <Bar data={this.generateChartData(dailyStats, 'Daily Misuse Count')}  />
                            </div>
                            <div style={{ padding: '20px' }}>
                                <h2 style={{ textAlign: 'center' }}>Monthly Misuse Statistics</h2>
                                <Bar data={this.generateChartData(monthlyStats, 'Monthly Misuse Count')} />
                            </div>
                            <div style={{ padding: '20px' }}>
                                <h2 style={{ textAlign: 'center' }}>Progress Over Time</h2>
                                <Line data={this.generateChartData(progressData, 'Misuse Progress')} />
                            </div>
                        </SwipeableViews>
                        <Pagination
                            dots={3}
                            index={index}
                            onChangeIndex={this.handleChangeIndex}
                        />
                    </Card>
                )}
            </div>
        );
    }
}

export default AssociationsContainer;
