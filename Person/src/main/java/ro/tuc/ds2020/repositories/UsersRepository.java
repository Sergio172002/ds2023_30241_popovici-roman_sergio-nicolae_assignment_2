package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.entities.Users;

import java.util.Optional;

public interface UsersRepository extends JpaRepository<Users, Integer> {

    /**
     * Example: JPA generate Query by Field
     */
    //Optional<Users> findByDeviceName(String deviceName);

    Optional<Users> findUsersByUsername(String username);

    /**
     * Example: Write Custom Query
     **/
//    @Query(value = "SELECT p " +
//            "FROM Person p " +
//            "WHERE p.username = :username " +
//            "AND p.age >= 60  ")
//    Optional<Person> findSeniorsByName(@Param("username") String username);
//
//
//    @Modifying
//    @Query("UPDATE Person p SET p.age = :newAge WHERE p.id = :uuid")
//    int updateNameByUuid(@Param("uuid") UUID uuid, @Param("newAge") Integer newAge);


}

