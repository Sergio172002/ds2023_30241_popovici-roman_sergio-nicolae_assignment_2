package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.entities.Ble;

import java.util.List;
import java.util.Optional;

public interface BleRepository extends JpaRepository<Ble, Integer> {

    /**
     * Example: JPA generate Query by Field
     */
    List<Ble> findByDeviceName(String deviceName);

    //static List<Ble> findAll();

    /**
     * Example: Write Custom Query
     **/
//    @Query(value = "SELECT p " +
//            "FROM Person p " +
//            "WHERE p.username = :username " +
//            "AND p.age >= 60  ")
//    Optional<Person> findSeniorsByName(@Param("username") String username);
//
//
//    @Modifying
//    @Query("UPDATE Person p SET p.age = :newAge WHERE p.id = :uuid")
//    int updateNameByUuid(@Param("uuid") UUID uuid, @Param("newAge") Integer newAge);


}

