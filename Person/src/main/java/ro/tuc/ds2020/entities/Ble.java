package ro.tuc.ds2020.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;

@Entity
public class Ble  implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    //@GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    //@Type(type = "uuid-binary")
    private Integer id;

    @Column(name = "rssi", nullable = false)
    private String rssi;

    @Column(name = "time", nullable = false)
    private String time;

    @Column(name = "device_name", nullable = false)
    private String deviceName;

    public Ble(){
    }
    public Ble(Integer id, String rssi, String time, String deviceName ){
        this.id = id;
        this.rssi = rssi;
        this.time = time;
        this.deviceName = deviceName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRssi() {
        return rssi;
    }

    public void setRssi(String rssi) {
        this.rssi = rssi;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
}
