package ro.tuc.ds2020.dtos;

public class LoginResponse {
    private UserDetailsDTO userDetails;
    private String token;

    public LoginResponse() {
    }

    public LoginResponse(UserDetailsDTO userDetails, String token) {
        this.userDetails = userDetails;
        this.token = token;
    }

    public LoginResponse(UserDetailsDTO userDetails) {
        this.userDetails = userDetails;
    }

    public UserDetailsDTO getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetailsDTO userDetails) {
        this.userDetails = userDetails;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
