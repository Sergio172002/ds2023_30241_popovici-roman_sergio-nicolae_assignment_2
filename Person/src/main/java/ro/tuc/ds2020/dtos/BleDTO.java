package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

public class BleDTO extends RepresentationModel<BleDTO> {
    private Integer id;
    private String rssi;
    private String time;
    private String deviceName;

    public BleDTO() {
    }

    public BleDTO(Integer id, String rssi, String time, String deviceName) {
        this.id = id;
        this.rssi = rssi;
        this.time = time;
        this.deviceName = deviceName;
    }

    public String getId() {
        return id.toString();
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRssi() {
        return rssi;
    }

    public void setRssi(String rssi) {
        this.rssi = rssi;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        BleDTO valuesDTO = (BleDTO) o;
        return Objects.equals(id, valuesDTO.id) && Objects.equals(rssi, valuesDTO.rssi) && Objects.equals(time, valuesDTO.time) && Objects.equals(deviceName, valuesDTO.deviceName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, rssi, time, deviceName);
    }
}
