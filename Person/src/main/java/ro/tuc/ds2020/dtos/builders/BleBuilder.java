package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.BleDetailsDTO;
import ro.tuc.ds2020.dtos.BleDTO;
import ro.tuc.ds2020.entities.Ble;

public class BleBuilder {

    private BleBuilder() {
    }

    public static BleDTO toBleDTO(Ble ble) {
        return new BleDTO(ble.getId(), ble.getRssi(), ble.getTime(), ble.getDeviceName());
    }

    public static BleDetailsDTO toBleDetailsDTO(Ble ble) {
        return new BleDetailsDTO(ble.getId(), ble.getRssi(), ble.getTime(), ble.getDeviceName());
    }

    public static Ble toEntity(BleDetailsDTO bleDetailsDTO) {
        return new Ble(bleDetailsDTO.getId(),
                bleDetailsDTO.getDeviceName(),
                bleDetailsDTO.getTime(),
                bleDetailsDTO.getRssi());
    }
}
