package ro.tuc.ds2020.dtos;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class BleDetailsDTO {

    private Integer id;
    @NotNull
    private String rssi;
    @NotNull
    private String time;
    @NotNull
    private String deviceName;

    public BleDetailsDTO() {
    }

    public BleDetailsDTO(String rssi, String time, String deviceName) {
        this.rssi = rssi;
        this.time = time;
        this.deviceName = deviceName;
    }

    public BleDetailsDTO(Integer id, String rssi, String time, String deviceName) {
        this.id = id;
        this.rssi = rssi;
        this.time = time;
        this.deviceName = deviceName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BleDetailsDTO that = (BleDetailsDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(rssi, that.rssi) && Objects.equals(time, that.time) && Objects.equals(deviceName, that.deviceName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, rssi, time, deviceName);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRssi() {
        return rssi;
    }

    public void setRssi(String rssi) {
        this.rssi = rssi;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
}
