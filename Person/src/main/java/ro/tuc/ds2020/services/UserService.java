package ro.tuc.ds2020.services;

import org.apache.catalina.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.BleDTO;
import ro.tuc.ds2020.dtos.BleDetailsDTO;
import ro.tuc.ds2020.dtos.UserDetailsDTO;
import ro.tuc.ds2020.dtos.builders.BleBuilder;
import ro.tuc.ds2020.dtos.builders.UserBuilder;
import ro.tuc.ds2020.entities.Ble;
import ro.tuc.ds2020.entities.Users;
import ro.tuc.ds2020.repositories.BleRepository;
import ro.tuc.ds2020.repositories.UsersRepository;

import javax.security.sasl.AuthenticationException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(BleService.class);
    private final UsersRepository userRepository;
    private final RestTemplate restTemplate;

    @Autowired
    public UserService(UsersRepository userRepository, RestTemplate restTemplate) {
        this.userRepository = userRepository;
        this.restTemplate = restTemplate;
    }

    public UserDetailsDTO login(String username, String password) throws AuthenticationException {
        Optional<Users> userOptional = userRepository.findUsersByUsername(username);

        if (!userOptional.isPresent()) {
            // Handle the case where the username is not found in the database
            LOGGER.error("Person with username {} was not found in db", username);
            throw new ResourceNotFoundException("Person with username: " + username);
        }

        Users user = userOptional.get();

        // Check if the provided password matches the stored password in the database
        if (!password.equals(user.getPassword())) {
            // Handle the case where the password is incorrect
            LOGGER.error("Incorrect password for Person with username: {}", username);
            throw new AuthenticationException("Incorrect password for username: " + username);
        }

        // If username and password match, create and return a PersonDetailsDTO
        return UserBuilder.toUserDetailsDTO(user);
    }


//    public List<BleDTO> findValues() {
//        List<Ble> bleList = bleRepository.findAll();
//        return bleList.stream()
//                .map(BleBuilder::toBleDTO)
//                .collect(Collectors.toList());
//    }
//
//    public BleDetailsDTO findPersonById(Integer id) {
//        Optional<Ble> prosumerOptional = bleRepository.findById(id);
//        if (!prosumerOptional.isPresent()) {
//            LOGGER.error("Person with id {} was not found in db", id);
//            throw new ResourceNotFoundException(Ble.class.getSimpleName() + " with id: " + id);
//        }
//        return BleBuilder.toBleDetailsDTO(prosumerOptional.get());
//    }

//    public PersonDetailsDTO findPersonByName(St id) {
//        Optional<Values> prosumerOptional = personRepository.findById(id);
//        if (!prosumerOptional.isPresent()) {
//            LOGGER.error("Person with id {} was not found in db", id);
//            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
//        }
//        return PersonBuilder.toPersonDetailsDTO(prosumerOptional.get());
//    }

//    public UUID insert(PersonDetailsDTO personDTO) {
//        Person person = PersonBuilder.toEntity(personDTO);
//        person = personRepository.save(person);
//        LOGGER.debug("Person with id {} was inserted in db", person.getId());
//        return person.getId();
//    }

//    public UUID insert(PersonDetailsDTO personDTO) {
//
//        UUID personUUID = UUID.randomUUID();
//
//        Person person = PersonBuilder.toEntity(personDTO);
//        person.setId(personUUID);
//        person = personRepository.save(person);
//        LOGGER.debug("Person with id {} was inserted in db", person.getId());
//        if(person.getRole().equals("USER")) {
//            String targetUrl = "http://172.16.238.6:8081/user";
//            HttpHeaders headers = new HttpHeaders();
//            headers.setContentType(MediaType.APPLICATION_JSON);
//
//            personDTO.setId(personUUID);
//            // Prepare the request entity with the necessary headers and body
//            HttpEntity<PersonDetailsDTO> requestEntity = new HttpEntity<>(personDTO, headers);
//
//            ResponseEntity<UUID> responseEntity = restTemplate.exchange(targetUrl, HttpMethod.POST, requestEntity, UUID.class);
//
//            // Handle the response as needed
//            UUID targetAppResponse = responseEntity.getBody();
//        }
//
//        return person.getId();
//    }

//    public UUID deleteById(UUID id) {
//        Optional<Person> prosumerOptional = personRepository.findById(id);
//
//        if (!prosumerOptional.isPresent()) {
//            LOGGER.error("Person with id {} was not found in db", id);
//            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
//        }else{
//            LOGGER.error("Person with id {} was deleted from the db", id);
//            personRepository.deleteById(id);
//
//            Person person = prosumerOptional.get();
//            if(person.getRole().equals("USER")){
//                String targetUrl = "http://172.16.238.6:8081/user/"+id;
//                HttpHeaders headers = new HttpHeaders();
//                headers.setContentType(MediaType.APPLICATION_JSON);
//
//                // Prepare the request entity with the necessary headers and body
//                HttpEntity<?> requestEntity = new HttpEntity<>(headers);
//
//                ResponseEntity<UUID> responseEntity = restTemplate.exchange(targetUrl, HttpMethod.DELETE, requestEntity, UUID.class);
//
//                // Handle the response as needed
//                // targetAppResponse = responseEntity.getBody();
//            }
//        }
//        return id;
//    }

//    @Transactional
//    public UUID updateAtId(UUID id, int age) {
//        Optional<Person> prosumerOptional = personRepository.findById(id);
//        if (!prosumerOptional.isPresent()) {
//            LOGGER.error("Person with id {} was not found in db", id);
//            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
//        }else{
//            LOGGER.error("Person with id {} was updated", id);
//            personRepository.updateNameByUuid(id, age);
//        }
//
//        // Create a simple JSON object with a key-value pair
//        return id;
//    }

//    //Complete Functions:.....
//    public PersonDetailsDTO login(String username, String password) throws AuthenticationException {
//        Optional<Person> personOptional = personRepository.findByUsername(username);
//
//        if (!personOptional.isPresent()) {
//            // Handle the case where the username is not found in the database
//            LOGGER.error("Person with username {} was not found in db", username);
//            throw new ResourceNotFoundException("Person with username: " + username);
//        }
//
//        Person person = personOptional.get();
//
//        // Check if the provided password matches the stored password in the database
//        if (!password.equals(person.getPassword())) {
//            // Handle the case where the password is incorrect
//            LOGGER.error("Incorrect password for Person with username: {}", username);
//            throw new AuthenticationException("Incorrect password for username: " + username);
//        }
//
//        // If username and password match, create and return a PersonDetailsDTO
//        return PersonBuilder.toPersonDetailsDTO(person);
//    }



}
