package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.BleDTO;
import ro.tuc.ds2020.dtos.BleDetailsDTO;
import ro.tuc.ds2020.services.BleService;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/ble")
public class BleController {

    private final BleService bleService;

    @Autowired
    public BleController(BleService bleService) {
        this.bleService = bleService;
    }

    //private JwtTokenValidator jwtTokenUtil;

    @GetMapping()
    public ResponseEntity<List<BleDTO>> getValues(@RequestHeader(value = "Authorization", required = false) String authorizationHeader) {

        // Check if the authorization header is present
//        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
//            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); // Return 401 Unauthorized if the header is not present or does not start with "Bearer "
//        }
//
//        String token = authorizationHeader.substring(7);
//        System.out.println(token);
//
//        if (!jwtTokenUtil.isTokenValid(token)) {
//            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); // Return 401 Unauthorized if the token is invalid
//        }

        List<BleDTO> dtos = bleService.findValues();
        for (BleDTO dto : dtos) {
            Link valuesLink = linkTo(methodOn(BleController.class)
                    .getValues(dto.getId())).withRel("valuesDetails");
            dto.add(valuesLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/unique-device-names")
    public ResponseEntity<List<String>> getUniqueDeviceNames() {
        List<String> uniqueDeviceNames = bleService.findUniqueDeviceNames();
        return ResponseEntity.ok(uniqueDeviceNames);
    }

    @GetMapping(value = "/{device_name}")
    public ResponseEntity<List<BleDetailsDTO>> getDataByDevice(@PathVariable("device_name") String deviceName) {

//       // Check if the authorization header is present
//        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
//            System.out.println("Nu am token!!");
//            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
//            // Return 401 Unauthorized if the header is not present or does not start with "Bearer "
//        }
//        String token = authorizationHeader.substring(7);
//        System.out.println(token);
//
//        if (!JwtTokenValidator.isTokenValid(token)) {
//            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); // Return 401 Unauthorized if the token is invalid
//        }

        List<BleDetailsDTO> dto = bleService.findBleByDeviceName(deviceName);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

//    @PostMapping()
//    public ResponseEntity<UUID> insertProsumer(@RequestHeader(value = "Authorization", required = false) String authorizationHeader, @Valid @RequestBody ValuesDetailsDTO personDTO) {
//
//        // Check if the authorization header is present
////        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
////            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); // Return 401 Unauthorized if the header is not present or does not start with "Bearer "
////        }
////
////        String token = authorizationHeader.substring(7);
////        System.out.println(token);
////
////        if (!jwtTokenUtil.isTokenValid(token)) {
////            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); // Return 401 Unauthorized if the token is invalid
////        }
//
//        UUID personID = personService.insert(personDTO);
//        return new ResponseEntity<>(personID, HttpStatus.CREATED);
//    }
//
//    @DeleteMapping(value = "/{id}")
//    public ResponseEntity<UUID> deleteUser(@RequestHeader(value = "Authorization", required = false) String authorizationHeader, @PathVariable("id") UUID personId){
//
//        // Check if the authorization header is present
//        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
//            System.out.println("Nu am token!!");
//            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
//            // Return 401 Unauthorized if the header is not present or does not start with "Bearer "
//        }
//        String token = authorizationHeader.substring(7);
//        System.out.println(token);
//
//        if (!JwtTokenValidator.isTokenValid(token)) {
//            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); // Return 401 Unauthorized if the token is invalid
//        }
//
//        UUID message = personService.deleteById(personId);
//        return new ResponseEntity<>(message, HttpStatus.ACCEPTED);
//    }
//
//    @PutMapping(value = "/{id}")
//    public ResponseEntity<UUID> updateAtUuid(@RequestHeader(value = "Authorization", required = false) String authorizationHeader, @PathVariable("id") UUID personId, @Valid @RequestBody UpdatePersonDTO updateValueDTO){
//
//        // Check if the authorization header is present
//        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
//            System.out.println("Nu am token!!");
//            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
//            // Return 401 Unauthorized if the header is not present or does not start with "Bearer "
//        }
//        String token = authorizationHeader.substring(7);
//        System.out.println(token);
//
//        if (!JwtTokenValidator.isTokenValid(token)) {
//            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); // Return 401 Unauthorized if the token is invalid
//        }
//
//        int newValue = updateValueDTO.getUpdatedValue();
//        System.out.println(newValue+" "+personId);
//        UUID message = personService.updateAtId(personId,newValue);
//        return new ResponseEntity<>(message,HttpStatus.ACCEPTED);
//    }
//
//    @GetMapping(value = "/{id}")
//    public ResponseEntity<ValuesDetailsDTO> getPerson(@PathVariable("id") UUID personId) {
//
////       // Check if the authorization header is present
////        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
////            System.out.println("Nu am token!!");
////            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
////            // Return 401 Unauthorized if the header is not present or does not start with "Bearer "
////        }
////        String token = authorizationHeader.substring(7);
////        System.out.println(token);
////
////        if (!JwtTokenValidator.isTokenValid(token)) {
////            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); // Return 401 Unauthorized if the token is invalid
////        }
//
//        ValuesDetailsDTO dto = personService.findPersonById(personId);
//        return new ResponseEntity<>(dto, HttpStatus.OK);
//    }
//
//    @PostMapping(value = "/login")  // Assuming you want to handle login at this endpoint
//    public ResponseEntity<LoginResponse> logInUser(@Valid @RequestBody LoginRequest loginRequest) throws AuthenticationException {
//        String username = loginRequest.getUsername();
//        String password = loginRequest.getPassword();
//        System.out.println("Sunt aici");
//
//        // Call the login method from your service to handle authentication
//        ValuesDetailsDTO personDetails = personService.login(username, password);
//
//        // Generate a JWT token
//        String token = generateJwtToken(username);
//
//        // Create a response object containing user details and the token
//        LoginResponse loginResponse = new LoginResponse(personDetails, token);
//
//        return new ResponseEntity<>(loginResponse, HttpStatus.OK);
//    }
//
//    private String generateJwtToken(String username) {
//        String secretKey = "aVeryLongStringThatContainsAMixOfUpperAndLowerCaseLettersNumbers1234567890AndSymbolsAABBCCDDEEFFGGHHIIJJKKLL"; // Replace with a strong secret key
//        long expirationMillis = 86400000; // Example: 1 day
//
//        return Jwts.builder()
//                .setSubject(username)
//                .setId(UUID.randomUUID().toString())
//                .setIssuedAt(new Date(System.currentTimeMillis()))
//                .setExpiration(new Date(System.currentTimeMillis() + expirationMillis))
//                .signWith(SignatureAlgorithm.HS512, secretKey)
//                .compact();
//    }


    //TODO: UPDATE per resource

}
